import sys
import re
import pickle
from xml.dom.minidom import parseString
import xml.etree.cElementTree as ET
import json

__notes__ = []
__title__ = None
__body__ = None


def new_note(title,body):
    global __notes__
    note = (title,body)
    __notes__.append(note)

def getnotes():
    global __notes__
    return __notes__


def delete_note(title):
    global __notes__
    for n in __notes__:
        if re.search("VLEAD",n[0]):
            __notes__.remove(n)
    

def find_notes_by_title(title):
    for n in __notes__:
        if re.search(title,n[0]):
            note_found = n[1] 
            print note_found


def find_notes_by_key_in_body(key):
    global __notes__
    for n in __notes__:
        if re.search(key,n[1]):
            print n[0], n[1]


def save_pickle():
    global __notes__
    fileObject = open("save.p", "wb")
    pickle.dump(__notes__, fileObject)
    fileObject.close()
    

def save_note_xml():
    global __notes__
    root = ET.Element("Master Note")
    for n in __notes__:    
        doc = ET.SubElement(root,"Note")
        field1 = ET.SubElement(doc,"Title")
        field1.text = n[0]
        field2 = ET.SubElement(doc,"Content")
        field2.text = n[1]
    tree = ET.ElementTree(root) 
    tree.write("Notes.xml")


def save_note_json():
    global __notes__
    with open('notes.text','w') as outfile:
        json.dump(__notes__,outfile)
