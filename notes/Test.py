import Notes


def run_test_Notes():
    def test_new_note():
        Notes.new_note("python","interpreted language")
        Notes.new_note("VLEAD","virtual labs")
        assert(Notes.getnotes() == "python","interpreted language")
        print "added a new note"
    
    
    def test_delete_note():
        global __notes__
        Notes.delete_note("VLEAD")
        assert(Notes.getnotes() != None)
        print "deleted a note"
        print "remaining notes", Notes.getnotes()

    def test_find_notes_by_title():
        Notes.find_notes_by_title("python")

    def test_find_notes_by_key_in_body():
        Notes.new_note("Dashboard","Stats of the labs")
        Notes.find_notes_by_key_in_body("Stats of the labs")

    
    def test_save_pickle():
        Notes.save_pickle()
       # (for unpickling)  notes_pickle = pickle.load(open("save.p", "rb"))
       # print "displaying notes in pickle format"
       # print notes_pickle

    def test_save_note_xml():
        Notes.save_note_xml()

   
    def test_save_note_json():
        Notes.save_note_json()


    test_new_note()
    test_delete_note()
    test_find_notes_by_title()
    test_find_notes_by_key_in_body()
    test_save_pickle()    
    test_save_note_xml()
    test_save_note_json()

run_test_Notes()
