import Clipboard
import Image


def run_clipboard_tests():

    def test_reset_clipboard():
        Clipboard.reset()
        assert(Clipboard.gettext() == None)
        assert(Clipboard.getblob() == None)


    def test_some_text():
        Clipboard.reset()
        Clipboard.puttext("This is Virtual Labs")
        text = Clipboard.gettext()
        assert( text == "This is Virtual Labs")
        
    
    def test_blob():
        Clipboard.reset()
        f = open("sandbucket.jpg")
        img = f.read()
        Clipboard.putblob(img)
        blob = Clipboard.getblob()
        assert(blob == img)



    test_reset_clipboard()
    test_some_text()
    test_blob()

run_clipboard_tests()




def run_clipboard_observer_tests():
    def test_one_observer():
        def anobserver(reason):
            print "observer notified. reason: ", reason

        Clipboard.reset()
        Clipboard.addobserver(anobserver)
        Clipboard.puttext("hello, world!")
    
    test_one_observer()

run_clipboard_observer_tests()
