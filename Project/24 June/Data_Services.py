from sqlalchemy import *
from sqlalchemy import func
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy import Column, Integer, String, DateTime, Boolean

engine = create_engine('mysql://root:04071991@localhost/dashboard_db')
connection = engine.connect()
Session = sessionmaker(bind=engine)
session = Session()
engine.dispose()



def getDeployedLabsByInstituteHandler(inst_id):
        query = text("Select * from deployed_labs where INST_ID = '" + inst_id + "'")
	try:
            result = connection.execute(query)
	    list_attributes =['LAB_ID', 'LAB_NAME', 'INST_ID', 'INST_NAME', 'DISC_ID', 'DISC_NAME', 'URL', 'SRC_CTRL_URL', '_TYPE',  				      'DESCRIPTION']
            list_final =[]
	    count=0
            for i in result:
                i = str(i).strip('()"') 
                count_1=0
                list_final.append({})
                for j in str(i).split(","):
                    list_final[count][ list_attributes[count_1] ]= j.strip('"')
                    count_1=count_1 + 1            
                count=count+1; 
            return list_final
        except:
            print 'inside except'        



def getDeployedLabsHandler():
        query = text("Select * from deployed_labs")
        try:
            result = connection.execute(query)
	    list_attributes =['LAB_ID', 'LAB_NAME', 'INST_ID', 'INST_NAME', 'DISC_ID', 'DISC_NAME', 'URL', 'SRC_CTRL_URL', '_TYPE',  				      'DESCRIPTION']
            list_final =[]
	    count=0
            for i in result:
                i = str(i).strip('()"') 
                count_1=0
                list_final.append({})
                for j in str(i).split(","):
                    list_final[count][ list_attributes[count_1] ]= j.strip('"')
                    count_1=count_1 + 1            
                count=count+1; 
            return list_final 
        except:
            print 'inside except'    



def getDeployedLabsByDisciplineHandler(disc_id):
        query = text("Select * from deployed_labs where DISC_ID = '" + disc_id + "'")
	try:
            result = connection.execute(query)
	    list_attributes =['LAB_ID', 'LAB_NAME', 'INST_ID', 'INST_NAME', 'DISC_ID', 'DISC_NAME', 'URL', 'SRC_CTRL_URL', '_TYPE',  				      'DESCRIPTION']
            list_final =[]
	    count=0
            for i in result:
                i = str(i).strip('()"') 
                count_1=0
                list_final.append({})
                for j in str(i).split(","):
                    list_final[count][ list_attributes[count_1] ]= j.strip('"')
                    count_1=count_1 + 1            
                count=count+1; 
            return list_final
        except:
            print 'inside except'        



def getParticipatingInstitutesHandler():
        query = text("Select INST_ID, INST_NAME from deployed_labs")
	try:
            result = connection.execute(query)
	    list_attributes =['INST_ID', 'INST_NAME']
            list_final =[]
	    count=0
            for i in result:
                i = str(i).strip('()"') 
                count_1=0
                list_final.append({})
                for j in str(i).split(","):
                    list_final[count][ list_attributes[count_1] ]= j.strip('"')
                    count_1=count_1 + 1            
                count=count+1; 
            return list_final
            #self.write(json.dumps(dict_final)) 
        except:
            print 'inside except'        



def getAvailableDisciplinesHandler():
        query = text("Select DISC_ID, DISC_NAME from deployed_labs")
	try:
            result = connection.execute(query)
	    list_attributes =['DISC_ID', 'DISC_NAME']
            list_final =[]
	    count=0
            for i in result:
                i = str(i).strip('()"') 
                count_1=0
                list_final.append({})
                for j in str(i).split(","):
                    list_final[count][ list_attributes[count_1] ]= j.strip('"')
                    count_1=count_1 + 1            
                count=count+1; 
            return list_final 
        except:
            print 'inside except'  



def getLabStatsHandler(lab_id):
        query = text("Select * from lab_stats where LAB_ID = '" + lab_id + "'")
        try:
            result = connection.execute(query)
	    list_attributes =['LAB_ID', 'LAB_NAME', 'RAM_QUOTA', 'RAM_USAGE', 'DISK_QUOTA', 'DISK_USAGE', 'UP_TIME', 'NUM_PROCESS', 'URL']
            list_final =[]
	    count=0
            for i in result:
                i = str(i).strip('()"') 
                count_1=0
                list_final.append({})
                for j in str(i).split(","):
                    list_final[count][ list_attributes[count_1] ]= j.strip('"')
                    count_1=count_1 + 1            
                count=count+1; 
            return list_final
        except:
            print 'inside except'        


