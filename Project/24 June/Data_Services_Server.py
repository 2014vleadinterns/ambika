import tornado.ioloop
import tornado.web
from tornado.options import define, options,parse_command_line
import tornado.autoreload
import os
import json
import Data_Services


        

class DeployedLabsByInstituteHandler(tornado.web.RequestHandler):
    def get(self,inst_id):
	obj = Data_Services.getDeployedLabsByInstituteHandler(inst_id)
        self.write(json.dumps(obj))



class DeployedLabsHandler(tornado.web.RequestHandler):
    def get(self):
        obj = Data_Services.getDeployedLabsHandler()
        self.write(json.dumps(obj))



class DeployedLabsByDisciplineHandler(tornado.web.RequestHandler):
    def get(self,disc_id):
        obj = Data_Services.getDeployedLabsByDisciplineHandler(disc_id)
        self.write(json.dumps(obj))

	

class ParticipatingInstitutesHandler(tornado.web.RequestHandler):
    def get(self):
	obj = Data_Services.getParticipatingInstitutesHandler()
        self.write(json.dumps(obj))



class AvailableDisciplinesHandler(tornado.web.RequestHandler):
    def get(self):
        obj = Data_Services.geAvailableDisciplinestHandler()
        self.write(json.dumps(obj))



class LabStatsHandler(tornado.web.RequestHandler):
    def get(self,lab_id):
        obj = Data_Services.getLabStatsHandler(lab_id)
        self.write(json.dumps(obj))



class LabSourceControlInfoHandler(tornado.web.RequestHandler):
    def get(self):
        self.write("This Page displays LabSourceControlInfo")




application = tornado.web.Application([
    (r"/getDeployedLabsByInstitute/([a-z]+)", DeployedLabsByInstituteHandler),
    (r"/getDeployedLabsByDiscipline/([a-z]+)", DeployedLabsByDisciplineHandler),
    (r"/getDeployedLabs", DeployedLabsHandler),
    (r"/getParticipatingInstitutes", ParticipatingInstitutesHandler),
    (r"/getAvailableDisciplines", AvailableDisciplinesHandler),
    (r"/getLabStats/([a-z]{3}[0-9]+)", LabStatsHandler),
    (r"/getLabSourceControlInfo", LabSourceControlInfoHandler),
    
])


if __name__ == "__main__":
    
    application.listen(9888)
    tornado.autoreload.start()
    for dir, _, files in os.walk('_Data_Services'):
        [tornado.autoreload.watch(dir + '/' + f) for f in files if not f.startswith('.')]
    
    tornado.ioloop.IOLoop.instance().start()
    
