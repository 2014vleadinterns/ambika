import tornado.ioloop
import tornado.web
from tornado.options import define, options,parse_command_line
import json
import random
import os
import tornado.autoreload


#define("port", default=4444, help="run on the given port", type=int)


class Dashboard(tornado.web.RequestHandler):
    def get(self):
	self.render("id.htm")

class Dashboard1(tornado.web.RequestHandler):     # Not being used currently
    def get(self, lab_id):
        self.render("id1.htm")

class WebHandler(tornado.web.RequestHandler):
    def get(self):
        self.write("This Page is down for maintenance")

class LabHandler(tornado.web.RequestHandler):
    def get(self):
        obj = [
		    {"lab_id": "cse02", "status": "running"},
		    {"lab_id": "ece34", "status": "stopped"},
		    {"lab_id": "civ04", "status": "running"},
                    {"lab_id": "eee45", "status": "stopped"},
                    {"lab_id": "mec23", "status": "running"},
                    {"lab_id": "cce01", "status": "stopped"},
                    {"lab_id": "mit03", "status": "stopped"}
                   
	      ]
        self.write(json.dumps(obj))  
       
class SearchHandler(tornado.web.RequestHandler):
    # def get(self):
    #   form = cgi.FieldStorage() 

    #   # Get data from fields
    #   first_name = form.getvalue('institute')
    #   last_name  = form.getvalue('discipline')

    #   print "Content-type:text/html\r\n\r\n"
    #   print "<html>"
    #   print "<head>"
    #   print "<title>Hello - Second CGI Program</title>"
    #   print "</head>"
    #   print "<body>"
    #   print "<h2>Hello %s %s</h2>" % (first_name, last_name)
    #   print "</body>"
    #   print "</html>"
    # def get(self):
    #     form = """<form method="post">
    #     <select name="Institute">
    #         <option name="IIITH">IIITH</option>
    #         <option name="IITD">IITD</option>
    #         <option name="IITG">IITG</option>
    #         </select>
            

    #     <input type="text" name="designation"/>
    #     <input type="submit"/>
    #     </form>"""
    #     self.write(form)
 
    def get(self):
        Institute = self.get_argument('Institute')
        discipline = self.get_argument('discipline')
        self.write("Institute: " + Institute + " discipline: " + discipline)


class Labinfo(tornado.web.RequestHandler):
    def get(self, lab_id):
        response = {
		     "lab_id": lab_id ,
                     "resources":{ 
                                   "ram_usage":random.random(),
                                   "disk_usage":random.random(),
                                   "cpu_units":random.randint(10,20),
                                   "up_time":random.randint(0,30),
                     },
                     "infrastructure":{
                                      "data_centre":"iiit",
                                      "os":"ubuntu"
                     },
                     "usage_analytics":{
                                        "hits_per_day":random.randint(20,100),
                                        "unique_hits_per_day":random.randint(0,30)
                     }
                   }
                           
        self.write(json.dumps(response))

application = tornado.web.Application([
   
    (r"/", WebHandler),
    (r"/labs", LabHandler),
    (r"/labs/([a-z]{3}[0-9]+)", Labinfo),
    (r"/dashboard", Dashboard),
    (r"/searchby",SearchHandler),
    (r"/dashboard/([a-z]{3}[0-9]+)", Dashboard1),
    (r'/GUI-dashboard/(.*)', tornado.web.StaticFileHandler, {'path': 'GUI-dashboard/'}),  # important for our css file
    (r'/(.*)', tornado.web.StaticFileHandler, {'path': '.'})       # important for our css file - how to serve a simple file using tornado
], debug = True,gzip=True)

if __name__ == "__main__":
    application.listen(9889)
    tornado.autoreload.start()
    for dir, _, files in os.walk('GUI-dashboard'):
        [tornado.autoreload.watch(dir + '/' + f) for f in files if not f.startswith('.')]
    
    tornado.ioloop.IOLoop.instance().start()
