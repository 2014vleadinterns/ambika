import tornado.ioloop
import tornado.web
from tornado.options import define, options,parse_command_line
import tornado.autoreload
import os


        

class DeployedLabsbyInstituteHandler(tornado.web.RequestHandler):
    def get(self):
        self.write("This Page DeployedLabsbyIns")


class DeployedLabsHandler(tornado.web.RequestHandler):
    def get(self):
        self.write("This Page displays DeployedLabs")


class DeployedLabsbyDisciplineHandler(tornado.web.RequestHandler):
    def get(self):
        self.write("This Page displays DeployedLabsbyDiscipline")


class ParticipatingInstitutesHandler(tornado.web.RequestHandler):
    def get(self):
        self.write("This Page displays ParticipatingInstitutes")


class AvailableDisciplinesHandler(tornado.web.RequestHandler):
    def get(self):
        self.write("This Page displays AvailDisciplines")


class LabStatsHandler(tornado.web.RequestHandler):
    def get(self):
        self.write("This Page displays LabStats")


class LabSourceControlInfoHandler(tornado.web.RequestHandler):
    def get(self):
        self.write("This Page displays LabSourceControlInfo")

application = tornado.web.Application([
    (r"/getDeployedLabsbyInstitute", DeployedLabsbyInstituteHandler),
    (r"/getDeployedLabsbyDiscipline", DeployedLabsbyDisciplineHandler),
    (r"/getDeployedLabs", DeployedLabsHandler),
    (r"/getParticipatingInstitutes", ParticipatingInstitutesHandler),
    (r"/getAvailableDisciplines", AvailableDisciplinesHandler),
    (r"/getLabStats", LabStatsHandler),
    (r"/getLabSourceControlInfo",LabSourceControlInfoHandler),
    
])


if __name__ == "__main__":
    
    application.listen(9888)
    tornado.autoreload.start()
    for dir, _, files in os.walk('_Data_Services'):
        [tornado.autoreload.watch(dir + '/' + f) for f in files if not f.startswith('.')]
    
    tornado.ioloop.IOLoop.instance().start()
    
